package by.anna.multicalculator;

import java.util.Scanner;

public class Multicalculator {
    public static void main(String[] args) {
        String numString = enterNum();
        int countNum = stringToInteger(numString);
        System.out.println("Enter " + countNum + " numbers:");
        int[] numbers = new int[countNum];
        for (int i = 0; i < countNum; i++) {
            System.out.println("Enter a number:");
            Scanner scanner = new Scanner(System.in);
            String someNumbers = scanner.nextLine();
            boolean isItNum = checkString(someNumbers);
            while (!isItNum) {
                System.out.println("Please, enter a number:");
                someNumbers = scanner.nextLine();
                isItNum = checkString(someNumbers);
            }
            numbers[i] = stringToInteger(someNumbers);
        }
        int sum = 0;
        for (int num : numbers) {
            sum = sum + num;
        }
        System.out.println("Sum of numbers: " + sum);
    }

    public static String enterNum() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many numbers would you like to enter?");
        String numString = scanner.nextLine();
        boolean isItNum = checkString(numString);
        while (!isItNum) {
            System.out.println("Please, enter a number:");
            numString = scanner.nextLine();
            isItNum = checkString(numString);
        }
        return numString;
    }

    public static int stringToInteger(String numbers) {
        return Integer.parseInt(numbers);
    }

    public static boolean checkString(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}