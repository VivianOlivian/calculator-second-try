package by.anna.wordcounter;

import java.util.Arrays;
import java.util.Scanner;

public class WordCounter {
    public static void main(String[] args) {
        String text = enterText();
        String[] words = splitWords(text);
        String word = enterWord();
        int count = 0;
        for (String wordsfromarray : words) {
            if (wordsfromarray.equalsIgnoreCase(word))
                count++;
        }
        System.out.println("Result is: " + count);
    }

    public static String enterText() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Inter your text: ");
        return scanner.nextLine();
    }

    public static String[] splitWords(String text) {
        String delimeter = " ";
        String[] words = text.split(delimeter);
        return words;
    }

    public static String enterWord() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Inter your word: ");
        return scanner.nextLine();
    }
}



