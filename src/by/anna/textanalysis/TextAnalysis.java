package by.anna.textanalysis;

import java.io.StringBufferInputStream;
import java.util.*;

public class TextAnalysis {
    public static void main(String[] args) {
        String sentence = enterSentence();
        System.out.println("Sentence is: " + sentence);
        String[] words = splitWords(sentence);
        countWords(words);
        sortWords(words);
        minAndMaxWord(words);
    }

    public static String enterSentence() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Inter sentence: ");
        return scanner.nextLine();
    }

    public static String[] splitWords(String sentence) {
        String delimeter = " ";
        String[] words = sentence.split(delimeter);
        return words;
    }

    public static void countWords(String[] words) {
        System.out.println("Entered " + words.length + " words");
    }

    public static void sortWords(String[] words) {
        Arrays.sort(words);
        System.out.println("Sort result: ");
        sortByLength(words);
        for (String array : words) {
            System.out.println(array);
        }
    }

    public static void sortByLength(String[] words) {
        for (int i = words.length - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (words[j].length() < words[j + 1].length()) {
                    String dummy = words[j];
                    words[j] = words[j + 1];
                    words[j + 1] = dummy;
                }
            }
        }
    }
    public static void minAndMaxWord(String[] words){
        System.out.println("Longest word is: " + words[0]);
        System.out.println("Shortest word is: " + words[words.length-1]);
    }
}