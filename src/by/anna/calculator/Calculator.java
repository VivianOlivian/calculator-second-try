package by.anna.calculator;

import java.util.Scanner;

 public class Calculator {
        public static void main(String[] args) {
            printToScreen("Старт программы", true);
            String numberOne = enterNum();
            String numberTwo = enterNum();
            printToScreen("Принял: ", false);
            printToScreen(numberOne, true);
            printToScreen("Принял: ", false);
            printToScreen(numberTwo, true);
            int firstNum = stringToInteger(numberOne);
            int secondNum = stringToInteger(numberTwo);
            int thirdNum = calcDifference(firstNum, secondNum);
            int fourthNum = calcSum(firstNum, secondNum);
            int fifthNum = calcMult(firstNum, secondNum);
            printToScreen("Результат: ", false);
            printToScreen(String.valueOf(thirdNum), true);
            printToScreen("Результат: ", false);
            printToScreen(String.valueOf(fourthNum), true);
            printToScreen("Результат: ", false);
            printToScreen(String.valueOf(fifthNum), true);
            printToScreen("Конец программы", false);
        }

        public static String enterNum() {
            Scanner scanner = new Scanner(System.in);
            printToScreen("Введите число:", false);
            String number = scanner.nextLine();
            boolean isItNum = checkString(number);
            while (!isItNum) {
                printToScreen("Введите число:", false);
                number = scanner.nextLine();
                isItNum = checkString(number);
            }
            return number;
        }

        public static int stringToInteger(String stringData) {
            return Integer.parseInt(stringData);
        }

        public static int calcDifference(int firstNumber, int secondNumber) {
            printToScreen("Начинаю считать", true);
            return firstNumber - secondNumber;
        }

        public static int calcSum(int firstNumber, int secondNumber) {
            printToScreen("Начинаю считать", true);
            return firstNumber + secondNumber;
        }

        public static int calcMult(int firstNumber, int secondNumber) {
            printToScreen("Начинаю считать", true);
            return firstNumber * secondNumber;
        }

        public static void printToScreen(String abraCadaBra, boolean addNewLine) {
            if (addNewLine) {
                System.out.println(abraCadaBra);
            } else {
                System.out.print(abraCadaBra);
            }
        }

        public static boolean checkString(String string) {
            try {
                Integer.parseInt(string);
                return true;
            } catch (IllegalArgumentException e) {
                return false;
            }
        }
    }

